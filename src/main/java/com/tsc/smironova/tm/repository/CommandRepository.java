package com.tsc.smironova.tm.repository;

import com.tsc.smironova.tm.api.repository.ICommandRepository;
import com.tsc.smironova.tm.constant.ArgumentConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConstant.ABOUT, ArgumentConstant.ARG_ABOUT, "Display developer info."
    );
    private static final Command VERSION = new Command(
            TerminalConstant.VERSION, ArgumentConstant.ARG_VERSION, "Display program version."
    );
    private static final Command HELP = new Command(
            TerminalConstant.HELP, ArgumentConstant.ARG_HELP, "Display list of terminal commands."
    );
    private static final Command INFO = new Command(
            TerminalConstant.INFO, ArgumentConstant.ARG_INFO, "Display system information."
    );
    private static final Command TASK_LIST = new Command(
            TerminalConstant.TASK_LIST, null, "Show task list."
    );
    private static final Command TASK_CREATE = new Command(
            TerminalConstant.TASK_CREATE, null, "Create new task."
    );
    private static final Command TASK_CLEAR = new Command(
            TerminalConstant.TASK_CLEAR, null, "Clear all tasks."
    );
    private static final Command TASK_VIEW_BY_ID = new Command(
            TerminalConstant.TASK_VIEW_BY_ID, null, "Show task by id."
    );
    private static final Command TASK_VIEW_BY_INDEX = new Command(
            TerminalConstant.TASK_VIEW_BY_INDEX, null, "Show task by index."
    );
    private static final Command TASK_VIEW_BY_NAME = new Command(
            TerminalConstant.TASK_VIEW_BY_NAME, null, "Show task by name."
    );
    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConstant.TASK_REMOVE_BY_ID, null, "Remove task by id."
    );
    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConstant.TASK_REMOVE_BY_INDEX, null, "Remove task by index."
    );
    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConstant.TASK_REMOVE_BY_NAME, null, "Remove task by name."
    );
    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConstant.TASK_UPDATE_BY_ID, null, "Update task by id."
    );
    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConstant.TASK_UPDATE_BY_INDEX, null, "Update task by index."
    );
    private static final Command TASK_START_BY_ID = new Command(
            TerminalConstant.TASK_START_BY_ID, null, "Start task by id."
    );
    private static final Command TASK_START_BY_INDEX = new Command(
            TerminalConstant.TASK_START_BY_INDEX, null, "Start task by index."
    );
    private static final Command TASK_START_BY_NAME = new Command(
            TerminalConstant.TASK_START_BY_NAME, null, "Start task by name."
    );
    private static final Command TASK_FINISH_BY_ID = new Command(
            TerminalConstant.TASK_FINISH_BY_ID, null, "Finish task status by id."
    );
    private static final Command TASK_FINISH_BY_INDEX = new Command(
            TerminalConstant.TASK_FINISH_BY_INDEX, null, "Finish task by index."
    );
    private static final Command TASK_FINISH_BY_NAME = new Command(
            TerminalConstant.TASK_FINISH_BY_NAME, null, "Finish task by name."
    );
    private static final Command TASK_UPDATE_STATUS_BY_ID = new Command(
            TerminalConstant.TASK_UPDATE_STATUS_BY_ID, null, "Update task status by id."
    );
    private static final Command TASK_UPDATE_STATUS_BY_INDEX = new Command(
            TerminalConstant.TASK_UPDATE_STATUS_BY_INDEX, null, "Update task status by index."
    );
    private static final Command TASK_UPDATE_STATUS_BY_NAME = new Command(
            TerminalConstant.TASK_UPDATE_STATUS_BY_NAME, null, "Update task status by name."
    );
    private static final Command PROJECT_LIST = new Command(
            TerminalConstant.PROJECT_LIST, null, "Show project list."
    );
    private static final Command PROJECT_CREATE = new Command(
            TerminalConstant.PROJECT_CREATE, null, "Create new project."
    );
    private static final Command PROJECT_CLEAR = new Command(
            TerminalConstant.PROJECT_CLEAR, null, "Clear all projects."
    );
    private static final Command PROJECT_VIEW_BY_ID = new Command(
            TerminalConstant.PROJECT_VIEW_BY_ID, null, "Show project by id."
    );
    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            TerminalConstant.PROJECT_VIEW_BY_INDEX, null, "Show project by index."
    );
    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            TerminalConstant.PROJECT_VIEW_BY_NAME, null, "Show project by name."
    );
    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConstant.PROJECT_REMOVE_BY_ID, null, "Remove project by id."
    );
    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConstant.PROJECT_REMOVE_BY_INDEX, null, "Remove project by index."
    );
    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConstant.PROJECT_REMOVE_BY_NAME, null, "Remove project by name."
    );
    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConstant.PROJECT_UPDATE_BY_ID, null, "Update project by id."
    );
    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConstant.PROJECT_UPDATE_BY_INDEX, null, "Update project by index."
    );
    private static final Command PROJECT_START_BY_ID = new Command(
            TerminalConstant.PROJECT_START_BY_ID, null, "Start project by id."
    );
    private static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConstant.PROJECT_START_BY_INDEX, null, "Start project by index."
    );
    private static final Command PROJECT_START_BY_NAME = new Command(
            TerminalConstant.PROJECT_START_BY_NAME, null, "Start project by name."
    );
    private static final Command PROJECT_FINISH_BY_ID = new Command(
            TerminalConstant.PROJECT_FINISH_BY_ID, null, "Finish project by id."
    );
    private static final Command PROJECT_FINISH_BY_INDEX = new Command(
            TerminalConstant.PROJECT_FINISH_BY_INDEX, null, "Finish project by index."
    );
    private static final Command PROJECT_FINISH_BY_NAME = new Command(
            TerminalConstant.PROJECT_FINISH_BY_NAME, null, "Finish project by name."
    );
    private static final Command PROJECT_UPDATE_STATUS_BY_ID = new Command(
            TerminalConstant.PROJECT_UPDATE_STATUS_BY_ID, null, "Update project status by id."
    );
    private static final Command PROJECT_UPDATE_STATUS_BY_INDEX = new Command(
            TerminalConstant.PROJECT_UPDATE_STATUS_BY_INDEX, null, "Update project status by index."
    );
    private static final Command PROJECT_UPDATE_STATUS_BY_NAME = new Command(
            TerminalConstant.PROJECT_UPDATE_STATUS_BY_NAME, null, "Update project status by name."
    );
    private static final Command COMMANDS = new Command(
            TerminalConstant.COMMANDS, null, "Show program commands."
    );
    private static final Command ARGUMENTS = new Command(
            TerminalConstant.ARGUMENTS, null, "Show program arguments."
    );
    private static final Command EXIT = new Command(
            TerminalConstant.EXIT, null, "Close application."
    );
    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, VERSION, HELP, INFO,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            TASK_VIEW_BY_ID, TASK_VIEW_BY_INDEX, TASK_VIEW_BY_NAME,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_NAME,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX, TASK_START_BY_NAME,
            TASK_FINISH_BY_ID, TASK_FINISH_BY_INDEX, TASK_FINISH_BY_NAME,
            TASK_UPDATE_STATUS_BY_ID, TASK_UPDATE_STATUS_BY_INDEX, TASK_UPDATE_STATUS_BY_NAME,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            PROJECT_VIEW_BY_ID, PROJECT_VIEW_BY_INDEX, PROJECT_VIEW_BY_NAME,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_NAME,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX, PROJECT_START_BY_NAME,
            PROJECT_FINISH_BY_ID, PROJECT_FINISH_BY_INDEX, PROJECT_FINISH_BY_NAME,
            PROJECT_UPDATE_STATUS_BY_ID, PROJECT_UPDATE_STATUS_BY_INDEX, PROJECT_UPDATE_STATUS_BY_NAME,
            COMMANDS, ARGUMENTS, EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
