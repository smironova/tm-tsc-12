package com.tsc.smironova.tm.service;

import com.tsc.smironova.tm.api.repository.IProjectRepository;
import com.tsc.smironova.tm.api.service.IProjectService;
import com.tsc.smironova.tm.enumerated.Status;
import com.tsc.smironova.tm.model.Project;
import com.tsc.smironova.tm.util.ValidationUtil;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project add(final String name, final String description) {
        if (ValidationUtil.isEmpty(name) || ValidationUtil.isEmpty(description))
            return null;
        final Project project = new Project(name, description);
        add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null)
            return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null)
            return;
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(String id) {
        if (ValidationUtil.isEmpty(id))
            return null;
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(Integer index) {
        if (ValidationUtil.checkIndex(index, projectRepository.size()))
            return null;
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(String name) {
        if (ValidationUtil.isEmpty(name))
            return null;
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project removeOneById(String id) {
        if (ValidationUtil.isEmpty(id))
            return null;
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project removeOneByIndex(Integer index) {
        if (ValidationUtil.checkIndex(index, projectRepository.size()))
            return null;
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByName(String name) {
        if (ValidationUtil.isEmpty(name))
            return null;
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) {
        if (ValidationUtil.isEmpty(id) || ValidationUtil.isEmpty(name) || ValidationUtil.isEmpty(description))
            return null;
        final Project project = findOneById(id);
        if (project == null)
            return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) {
        if (ValidationUtil.checkIndex(index, projectRepository.size()) || ValidationUtil.isEmpty(name) || ValidationUtil.isEmpty(description))
            return null;
        final Project project = findOneByIndex(index);
        if (project == null)
            return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final String id) {
        if (ValidationUtil.isEmpty(id))
            return null;
        final Project project = findOneById(id);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(final Integer index) {
        if (ValidationUtil.checkIndex(index, projectRepository.size()))
            return null;
        final Project project = findOneByIndex(index);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final String name) {
        if (ValidationUtil.isEmpty(name))
            return null;
        final Project project = findOneByName(name);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishProjectById(final String id) {
        if (ValidationUtil.isEmpty(id))
            return null;
        final Project project = findOneById(id);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final Integer index) {
        if (ValidationUtil.checkIndex(index, projectRepository.size()))
            return null;
        final Project project = findOneByIndex(index);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishProjectByName(final String name) {
        if (ValidationUtil.isEmpty(name))
            return null;
        final Project project = findOneByName(name);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        if (ValidationUtil.isEmpty(id) || status == null)
            return null;
        final Project project = findOneById(id);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        if (ValidationUtil.checkIndex(index, projectRepository.size()) || status == null)
            return null;
        final Project project = findOneByIndex(index);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(final String name, final Status status) {
        if (ValidationUtil.isEmpty(name) || status == null)
            return null;
        final Project project = findOneByName(name);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

}
