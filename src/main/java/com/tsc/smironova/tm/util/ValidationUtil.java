package com.tsc.smironova.tm.util;

public interface ValidationUtil {

    static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

    static boolean checkIndex(final Integer index, final int size) {
        return index == null || index < 0 || index > size - 1;
    }

}
