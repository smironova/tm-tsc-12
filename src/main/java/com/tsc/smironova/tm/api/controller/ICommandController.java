package com.tsc.smironova.tm.api.controller;

public interface ICommandController {

    void showIncorrectArgument();

    void showIncorrectCommand();

    void showAbout();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

    void showInfo();

    void exit();

}
