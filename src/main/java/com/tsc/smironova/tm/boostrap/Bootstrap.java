package com.tsc.smironova.tm.boostrap;

import com.tsc.smironova.tm.api.controller.ICommandController;
import com.tsc.smironova.tm.api.controller.IProjectController;
import com.tsc.smironova.tm.api.controller.ITaskController;
import com.tsc.smironova.tm.api.repository.ICommandRepository;
import com.tsc.smironova.tm.api.repository.IProjectRepository;
import com.tsc.smironova.tm.api.repository.ITaskRepository;
import com.tsc.smironova.tm.api.service.ICommandService;
import com.tsc.smironova.tm.api.service.IProjectService;
import com.tsc.smironova.tm.api.service.ITaskService;
import com.tsc.smironova.tm.constant.ArgumentConstant;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.controller.CommandController;
import com.tsc.smironova.tm.controller.ProjectController;
import com.tsc.smironova.tm.controller.TaskController;
import com.tsc.smironova.tm.repository.CommandRepository;
import com.tsc.smironova.tm.repository.ProjectRepository;
import com.tsc.smironova.tm.repository.TaskRepository;
import com.tsc.smironova.tm.service.CommandService;
import com.tsc.smironova.tm.service.ProjectService;
import com.tsc.smironova.tm.service.TaskService;
import com.tsc.smironova.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(this.commandRepository);
    private final ICommandController commandController = new CommandController(this.commandService);
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(this.taskRepository);
    private final ITaskController taskController = new TaskController(this.taskService);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(this.projectRepository);
    private final IProjectController projectController = new ProjectController(this.projectService);

    public void run(final String... args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    public void parseCommand(final String command) {
        if (command == null)
            return;
        switch (command) {
            case TerminalConstant.ABOUT -> this.commandController.showAbout();
            case TerminalConstant.VERSION -> this.commandController.showVersion();
            case TerminalConstant.HELP -> this.commandController.showHelp();
            case TerminalConstant.INFO -> this.commandController.showInfo();
            case TerminalConstant.TASK_LIST -> this.taskController.showTaskList();
            case TerminalConstant.TASK_CREATE -> this.taskController.createTask();
            case TerminalConstant.TASK_CLEAR -> this.taskController.clearTask();
            case TerminalConstant.TASK_VIEW_BY_ID -> this.taskController.showTaskById();
            case TerminalConstant.TASK_VIEW_BY_INDEX -> this.taskController.showTaskByIndex();
            case TerminalConstant.TASK_VIEW_BY_NAME -> this.taskController.showTaskByName();
            case TerminalConstant.TASK_REMOVE_BY_ID -> this.taskController.removeTaskById();
            case TerminalConstant.TASK_REMOVE_BY_INDEX -> this.taskController.removeTaskByIndex();
            case TerminalConstant.TASK_REMOVE_BY_NAME -> this.taskController.removeTaskByName();
            case TerminalConstant.TASK_UPDATE_BY_ID-> this.taskController.updateTaskById();
            case TerminalConstant.TASK_UPDATE_BY_INDEX -> this.taskController.updateTaskByIndex();
            case TerminalConstant.TASK_START_BY_ID -> this.taskController.startTaskById();
            case TerminalConstant.TASK_START_BY_INDEX -> this.taskController.startTaskByIndex();
            case TerminalConstant.TASK_START_BY_NAME -> this.taskController.startTaskByName();
            case TerminalConstant.TASK_FINISH_BY_ID -> this.taskController.finishTaskById();
            case TerminalConstant.TASK_FINISH_BY_INDEX -> this.taskController.finishTaskByIndex();
            case TerminalConstant.TASK_FINISH_BY_NAME -> this.taskController.finishTaskByName();
            case TerminalConstant.TASK_UPDATE_STATUS_BY_ID -> this.taskController.changeTaskStatusById();
            case TerminalConstant.TASK_UPDATE_STATUS_BY_INDEX -> this.taskController.changeTaskStatusByIndex();
            case TerminalConstant.TASK_UPDATE_STATUS_BY_NAME -> this.taskController.changeTaskStatusByName();
            case TerminalConstant.PROJECT_LIST -> this.projectController.showProjectList();
            case TerminalConstant.PROJECT_CREATE -> this.projectController.createProject();
            case TerminalConstant.PROJECT_CLEAR -> this.projectController.clearProject();
            case TerminalConstant.PROJECT_VIEW_BY_ID -> this.projectController.showProjectById();
            case TerminalConstant.PROJECT_VIEW_BY_INDEX -> this.projectController.showProjectByIndex();
            case TerminalConstant.PROJECT_VIEW_BY_NAME -> this.projectController.showProjectByName();
            case TerminalConstant.PROJECT_REMOVE_BY_ID -> this.projectController.removeProjectById();
            case TerminalConstant.PROJECT_REMOVE_BY_INDEX -> this.projectController.removeProjectByIndex();
            case TerminalConstant.PROJECT_REMOVE_BY_NAME -> this.projectController.removeProjectByName();
            case TerminalConstant.PROJECT_UPDATE_BY_ID-> this.projectController.updateProjectById();
            case TerminalConstant.PROJECT_UPDATE_BY_INDEX -> this.projectController.updateProjectByIndex();
            case TerminalConstant.PROJECT_START_BY_ID -> this.projectController.startProjectById();
            case TerminalConstant.PROJECT_START_BY_INDEX -> this.projectController.startProjectByIndex();
            case TerminalConstant.PROJECT_START_BY_NAME -> this.projectController.startProjectByName();
            case TerminalConstant.PROJECT_FINISH_BY_ID -> this.projectController.finishProjectById();
            case TerminalConstant.PROJECT_FINISH_BY_INDEX -> this.projectController.finishProjectByIndex();
            case TerminalConstant.PROJECT_FINISH_BY_NAME -> this.projectController.finishProjectByName();
            case TerminalConstant.PROJECT_UPDATE_STATUS_BY_ID-> this.projectController.changeProjectStatusById();
            case TerminalConstant.PROJECT_UPDATE_STATUS_BY_INDEX-> this.projectController.changeProjectStatusByIndex();
            case TerminalConstant.PROJECT_UPDATE_STATUS_BY_NAME-> this.projectController.changeProjectStatusByName();
            case TerminalConstant.COMMANDS -> this.commandController.showCommands();
            case TerminalConstant.ARGUMENTS -> this.commandController.showArguments();
            case TerminalConstant.EXIT -> this.commandController.exit();
            default -> this.commandController.showIncorrectArgument();
        }
    }

    public void parseArg(final String arg) {
        if (arg == null)
            return;
        switch (arg) {
            case ArgumentConstant.ARG_ABOUT -> this.commandController.showAbout();
            case ArgumentConstant.ARG_VERSION -> this.commandController.showVersion();
            case ArgumentConstant.ARG_HELP -> this.commandController.showHelp();
            case ArgumentConstant.ARG_INFO ->  this.commandController.showInfo();
            default -> this.commandController.showIncorrectCommand();
        }
    }

    public void parseArgs(final String[] args) {
        if (args == null || args.length == 0)
            return;
        final String arg = args[0];
        parseArg(arg);
        this.commandController.exit();
    }

}
